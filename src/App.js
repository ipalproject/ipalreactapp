import React from "react";
import {
    BrowserRouter as Router,
    Route,
    Routes,
    Navigate,
} from "react-router-dom";
import Quiz from "./pages/quiz";
import Dashboard from "./pages/dashboard";
import Login from "./pages/login";
import Register from "./pages/register";
import Analytics from "./pages/analytics";
import Improvement from "./pages/improvementTest";
import Resources from "./pages/resources";
import Result from "./pages/result";

function App() {
    return (
        <Router>
            <Routes>
                <Route path="/dashboard" element={<Dashboard />} />
                <Route path="/quiz" element={<Quiz />} />
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route path="/analytics" element={<Analytics />} />
                <Route path="/improvement" element={<Improvement />} />
                <Route path="/resources" element={<Resources />} />
                <Route path="/result" element={<Result />} />
                <Route path="/*" element={<Navigate to="/dashboard" />} />
            </Routes>
        </Router>
    );
}

export default App;
