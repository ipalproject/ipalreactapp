import React from "react";
import { Link } from "react-router-dom";

class Result extends React.Component {
    state = {
        loading: true,
        data: [],
    };

    componentDidMount() {
        console.log("User => ", localStorage.getItem("userName"));
        if (!("userName" in localStorage))
          window.location.href = "/login";
        this.stopLoading();
    }

    stopLoading() {
        this.setState({ loading: false });
    }

    logOut() {
        localStorage.removeItem("userID");
        localStorage.removeItem("userName");
    }

    render() {
        if (!this.state.loading)
            return (
                <div>
                    <div className="sidebar">
                        <div className="logo-details">
                            <i className="bx bxs-graduation"></i>
                            <span className="logo_name">iPal</span>
                        </div>
                        <ul className="nav-links">
                            <li>
                                <a
                                    href="/#"
                                    tooltip="Dashboard"
                                    className=""
                                >
                                    <i className="bx bx-grid-alt"></i>
                                    <span className="links_name">
                                        Dashboard
                                    </span>
                                </a>
                            </li>
                            <li>
                            <a
                                    href="/resources"
                                    tooltip="Resources"
                                    className=""
                                >
                                    <i className="bx bx-grid-alt"></i>
                                    <span className="links_name">
                                        Resources
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-abacus"></i>
                                    <Link to="/quiz" className="links_name">
                                        Take a Test
                                    </Link>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-pie-chart-alt-2"></i>
                                    <Link to="/analytics" className="links_name">
                                        Analytics
                                    </Link>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-cog"></i>
                                    <Link to="/improvement" className="links_name">
                                        Improvement Test
                                    </Link>
                                </a>
                            </li>
                            <li className="log_out">
                                <a href="/#">
                                    <i className="bx bx-log-out"></i>
                                    <span className="links_name" onClick={this.logOut}>Log out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <section className="home-section">
                        <nav>
                            <div className="sidebar-button">
                                <span className="dashboard">Result</span>
                            </div>
                            
                            <div className="profile-details">
                                <span className="admin_name">{localStorage.getItem("userName")}</span>
                            </div>
                        </nav>
                        <div className="home-content">
                            <div className="sales-boxes">
                                <div className="recent-sales box">
                                    <div className="title">Your marks :</div>
                                    <div className="title">
                                        <b>30/45</b>
                                    </div>
                                </div>
                                <div className="recent-sales box">
                                    <div className="title">Subject Name :</div>
                                    <div className="sales-details">
                                        <ol>
                                            <li>Resource Link</li>
                                            <li>Resource Link</li>
                                            <li>Resource Link</li>
                                            <li>Resource Link</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            );
        else return <div class="loading_screen">
            <div class="loading_image"></div>
        </div>;
    }
}

export default Result;
