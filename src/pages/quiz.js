import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";

class Quiz extends React.Component {
    state = {
        loading: true,
        page: 0,
        data: [],
        result: [],
        code: [],
        subjects: []
    };

    componentDidMount() {
        if (!("userName" in localStorage)) window.location.href = "/login";
        let arr = [];
        for(let i=0;i<27;i++)
            arr.push('n');
        this.setState({result: arr});
        let carr = [];
        for(let i=0;i<27;i++)
            carr.push('x');
        this.setState({code: carr});

        let questionList = [];
        this.getSubjects().then((data) => {
            if (data) {
                data.forEach((item) => {
                    this.getQuestions(item["Subject_Code"]).then((data) => {
                        questionList.push(...data);
                        if (questionList.length === 27) {
                            this.stopLoading();
                            this.setState({ data: questionList });
                            console.log("Data => ", questionList);
                        }
                    });
                });
            } else console.log("Error in getting Data");
        });
    }

    stopLoading() {
        this.setState({ loading: false });
    }

    logOut() {
        localStorage.removeItem("userID");
        localStorage.removeItem("userName");
    }

    getSubjects = () => {
        const subjects = axios({
            url: "http://127.0.0.1:8000/api/getsubjects",
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods":
                    "GET,PUT,POST,DELETE,PATCH,OPTIONS",
                "Content-Type": "application/json",
            },
        })
            .then((res) => {
                this.setState({subjects: res.data.subjects});
                return res.data.subjects;
            })
            .catch((err) => {
                if (err.response?.data) {
                    console.log("Error", err.response.data);
                    return null;
                } else {
                    console.log("Error", err);
                    return null;
                }
            });
        return subjects;
    };

    getQuestions = (subject) => {
        const questions = axios({
            url: "http://127.0.0.1:8000/api/getquestions",
            method: "POST",
            data: {
                subject: subject,
            },
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods":
                    "GET,PUT,POST,DELETE,PATCH,OPTIONS",
                "Content-Type": "application/json",
            },
        })
            .then((res) => {
                return res.data.message;
            })
            .catch((err) => {
                if (err.response?.data) {
                    console.log("Error", err.response.data);
                    return null;
                } else {
                    console.log("Error", err);
                    return null;
                }
            });
        return questions;
    };

    handleNextClick = () => {
        if (this.state.page < 2) 
            this.setState({ page: this.state.page + 1 });
        else {
            let arr =[]
            this.state.subjects.forEach((item) => {
                arr[item["Subject_Code"]] = 0;
            })

            this.state.code.map((item, index) => {
                if(item === 'x')
                    return
                else {
                    if(this.state.result[index] === 'y')
                        arr[item] = arr[item] + 5;
                    else
                        arr[item] = arr[item] -3;
                }
            })

            Object.keys(arr).map((item, index) => {
                let data = {
                    'User_ID': localStorage.getItem('userID'),
                    'Subject_Code': item,
                    'Marks': (arr[item]/15)*100
                }
                if(index === 8)
                    this.updateReport(data, true);
                else
                    this.updateReport(data);
            })
        }
    };

    updateReport = (data, flag = false) => {
        axios({
            url: "http://127.0.0.1:8000/api/report",
            method: "POST",
            data: data,
            headers: {
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
                'Content-Type' : 'application/json'
            },
        }).then((res) => {
            console.log(res.data)
            if(flag)
                window.location.href = '/dashboard';
        }).catch((err) => {
            if(err.response?.data)
                console.log("Error", err.response.data);
            else
                console.log("Error", err);
        })
    }

    handleBackClick = () => {
        this.setState({ page: this.state.page - 1 });
    };

    handleOptionClick = (e, correct, code, key) => {
        console.log(e.target.value);
        const newCarr = this.state.code.map((item, index) => {
            if(index === key)
                return code;
            else
                return item;
        })
        this.setState({code: newCarr});

        const newRarr = this.state.result.map((item, index) => {
            if(index === key) {
                if(e.target.value === correct)
                    return 'y';
                else
                    return 'n';
            }
            else
                return item;
        })
        this.setState({result: newRarr});
    }

    renderQuestions() {
        return (
            <div className="home-content">
                {this.state.data.map((item, key) => {
                    if (
                        key >= this.state.page * 10 &&
                        key < this.state.page * 10 + 10
                    ) {
                        return (
                            <div className="sales-boxes" key={key}>
                                <div className="question-box">
                                    <div className="title">
                                        Q{key + 1}) {item["Question"]}
                                    </div>
                                    <br />
                                    <div className="sales-details">
                                        <form onChange={(e) => this.handleOptionClick(e, item["Correct_Option"], item["Subject_Code"], key)}>
                                            <input
                                                type="radio"
                                                name="fav_language"
                                                value="A"
                                            />
                                            <label>
                                                {item["OptionA"]}
                                            </label>
                                            <br />
                                            <input
                                                type="radio"
                                                name="fav_language"
                                                value="B"
                                            />
                                            <label>
                                                {item["OptionB"]}
                                            </label>
                                            <br />
                                            <input
                                                type="radio"
                                                name="fav_language"
                                                value="C"
                                            />
                                            <label>
                                                {item["OptionC"]}
                                            </label>
                                            <br />
                                            <input
                                                type="radio"
                                                name="fav_language"
                                                value="D"
                                            />
                                            <label>
                                                {item["OptionD"]}
                                            </label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        );
                    }
                })}
                <div className="naviHolder">
                    <button
                        className="naviButtons"
                        onClick={this.handleNextClick}
                    >
                        {this.state.page === 2 ? "Submit" : "Next"}
                    </button>
                    <button
                        className="naviButtons"
                        onClick={this.handleBackClick}
                        style={{
                            display: this.state.page === 0 ? "none" : "block",
                        }}
                    >
                        Back
                    </button>
                </div>
            </div>
        );
    }
    render() {
        return (
            <div>
                <div className="sidebar">
                    <div className="logo-details">
                        <i className="bx bxs-graduation"></i>
                        <span className="logo_name">iPal</span>
                    </div>
                    <ul className="nav-links">
                        <li>
                            <a href="/#">
                                <i className="bx bx-grid-alt"></i>
                                <Link to="/dashboard" className="links_name">
                                    Dashboard
                                </Link>
                            </a>
                        </li>
                        <li>
                            <a href="/#">
                                <i className="bx bx-book"></i>
                                <span className="links_name">Resources</span>
                            </a>
                        </li>
                        <li>
                            <a href="/#" className="active">
                                <i className="bx bx-abacus"></i>
                                <Link to="/quiz" className="links_name">
                                    Take a Test
                                </Link>
                            </a>
                        </li>
                        <li>
                            <a href="/#">
                                <i className="bx bx-pie-chart-alt-2"></i>
                                <Link to="/analytics" className="links_name">
                                    Analytics
                                </Link>
                            </a>
                        </li>
                        <li>
                            <a href="/#">
                                <i className="bx bx-cog"></i>
                                <Link to="/improvement" className="links_name">
                                    Improvement Test
                                </Link>
                            </a>
                        </li>
                        <li className="log_out">
                            <a href="/#">
                                <i className="bx bx-log-out"></i>
                                <span
                                    className="links_name"
                                    onClick={this.logOut}
                                >
                                    Log out
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <section className="home-section">
                    <nav>
                        <div className="sidebar-button">
                            <span className="assessment">Assessment</span>
                        </div>
                        <div className="profile-details">
                            <span className="admin_name">
                                {localStorage.getItem("userName")}
                            </span>
                        </div>
                    </nav>
                    {this.state.loading ? (
                        <div className="loading_screen">
                            <div className="loading_image"></div>
                        </div>
                    ) : (
                        this.renderQuestions()
                    )}
                </section>
            </div>
        );
    }
}

export default Quiz;
