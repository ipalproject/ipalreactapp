import React from "react";
import { Link } from "react-router-dom";
import {faker} from '@faker-js/faker';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';
  import { Line } from 'react-chartjs-2';

  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );
    
const labels = ['Test Attempt 1', 'Test Attempt 2', 'Test Attempt 3', 'Test Attempt 4', 'Test Attempt 5'];
class Analytics extends React.Component {
    state = {
        loading: true,
        data: [],
        random_value: 0,
    };


    componentDidMount() {
        if (localStorage.getItem("userName") === null)
        window.location.href = "/login";
        this.stopLoading();
    }

    stopLoading() {
        this.setState({ loading: false });
    }

    logOut() {
        localStorage.removeItem("userID");
        localStorage.removeItem("userName");
    }

    render() {

        const setValues = (e) => {
            this.setState({ random_value: faker.datatype.number({ min: 0, max: 100 }) });
        }
        const options_for_totalmarks = {
            responsive: true,
            plugins: {
              legend: {
                position: 'top',
              },
              title: {
                display: true,
                text: 'Total Marks',
              },
            },
        };
        const options_for_subject = {
            responsive: true,
            plugins: {
              legend: {
                position: 'top',
              },
              title: {
                display: true,
                text: 'Subject',
              },
            },
        };

        const data_for_totalmarks = {
            labels,
            datasets: [
              {
                label: 'Total Marks',
                data: [70,64,62,70,74],
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgba(255, 99, 132, 0.5)',
              },
            ],
        };


        const data_for_subject = {
            labels,
            datasets: [
              {
                label: 'Subject Marks',
                data: labels.map(() => faker.datatype.number({ min: 0, max: 100 })),
                borderColor: 'rgba(120, 255, 69, 0.91)',
                backgroundColor: 'rgba(120, 255, 69, 0.41)',
              },
            ],
        };

        if (!this.state.loading)
            return (
                <div>
                    <div className="sidebar">
                        <div className="logo-details">
                            <i className="bx bxs-graduation"></i>
                            <span className="logo_name">iPal</span>
                        </div>
                        <ul className="nav-links">
                            <li>
                                <a href="/#">
                                    <i className="bx bx-grid-alt"></i>
                                    <Link
                                        to="/dashboard"
                                        className="links_name"
                                    >
                                        Dashboard
                                    </Link>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-book"></i>
                                    <span className="links_name">
                                        Resources
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-abacus"></i>
                                    <Link to="/quiz" className="links_name">
                                        Take a Test
                                    </Link>
                                </a>
                            </li>
                            <li>
                                <a href="/#" className="active">
                                    <i className="bx bx-pie-chart-alt-2"></i>
                                    <span className="links_name">
                                        Analytics
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-cog"></i>
                                    <Link to="/improvement" className="links_name">
                                        Improvement Test
                                    </Link>
                                </a>
                            </li>
                            <li className="log_out">
                                <a href="/#">
                                    <i className="bx bx-log-out"></i>
                                    <span className="links_name" onClick={this.logOut}>Log out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <section className="home-section">
                        <nav>
                            <div className="sidebar-button">
                                <span className="assessment">Analytics</span>
                            </div>
                            <div className="profile-details">
                                <span className="admin_name">{localStorage.getItem("userName")}</span>
                            </div>
                        </nav>
                        <div className="push-down-mahn"></div>
                        <div className="home-content">
                            <div className="sales-boxes">
                                <div className="question-box">
                                    
                                <Line options={options_for_totalmarks} data={data_for_totalmarks} />

                                </div>
                            </div>
                            <div className="sales-boxes">
                                <div className="question-box">
                                    
                                <Line options={options_for_subject} data={data_for_subject} />
                                <br />
                                <label for="subject">Choose a subject : </label>

                                <select name="subject" id="subject" onChange={setValues}>
                                <option value="os">Operating Systems</option>
                                <option value="ds">Data Structures</option>
                                <option value="CSA">Computer System Architecture</option>
                                <option value="dbms">Database Management Systems</option>
                                </select>

                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            );
        else return <div>Loading...</div>;
    }
}

export default Analytics;
