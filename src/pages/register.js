import axios from "axios";
import React from "react";
import { Link } from "react-router-dom";

class Register extends React.Component {
    state = {
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
        message: "",
    };

    render() {
        const setName = (e) => {
            this.setState({ name: e.target.value });
        };

        const setEmail = (e) => {
            this.setState({ email: e.target.value });
        };

        const setPassword = (e) => {
            this.setState({ password: e.target.value });
        };

        const setConfirmPassword = (e) => {
            this.setState({ confirmPassword: e.target.value });
        };

        const validatePass = (e) => {
            if(this.state.password !== this.state.confirmPassword)
                this.setState({message: "Passwords don't match"});
            else {
                this.setState({message: ""});
            }
        }
        const registerUser = (e) => {
            e.preventDefault();

            if(this.state.password !== this.state.confirmPassword)
                alert("Passwords don't match");
            else {
                const data = {
                    'Name': this.state.name,
                    'Email': this.state.email,
                    'Password': this.state.password
                }

                axios({
                    url: "http://127.0.0.1:8000/api/register",
                      method: "POST",
                    data: data,
                    headers: {
                        'Access-Control-Allow-Origin' : '*',
                        'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
                        'Content-Type' : 'application/json'
                    },
                }).then((res) => {
                    localStorage.setItem('userName', this.state.name);
                    window.location.href = '/dashboard';
                }).catch((err) => {
                    if(err.response?.data)
                        console.log("Error", err.response.data);
                    else
                        console.log("Error", err);
                })
            }
        }

        return (
            <div class="login-wrapper">
                <div id="bg"></div>
                <form class="form">
                    <div class="form-field username">
                        <input
                            type="text"
                            placeholder="Username"
                            value={this.state.name}
                            onChange={setName}
                            required
                        />
                    </div>

                    <div class="form-field email">
                        <input
                            type="email"
                            placeholder="E-mail"
                            value={this.state.email}
                            onChange={setEmail}
                            required
                        />
                    </div>

                    <div class="form-field password">
                        <input
                            type="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={setPassword}
                            onKeyUp={validatePass}
                            required
                        />
                    </div>

                    <div class="form-field password">
                        <input
                            type="password"
                            placeholder="Re-enter Password"
                            value={this.state.confirmPassword}
                            onChange={setConfirmPassword}
                            onKeyUp={validatePass}
                            required
                        />
                    </div>
                    <p class="warning"><u>{this.state.message}</u></p>
                    <p>
                        Already have an account? <Link to="/login">Login</Link>{" "}
                        here.
                    </p>
                    <div class="form-field">
                        <button class="btn" type="submit" onClick={registerUser}>
                            Register
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

export default Register;
