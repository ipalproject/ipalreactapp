import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";

class Login extends React.Component {
    state = {
        email: "",
        password: "",
    };

    render() {
        const setEmail = (e) => {
            this.setState({ email: e.target.value });
        };

        const setPassword = (e) => {
            this.setState({ password: e.target.value });
        };

        const loginUser = (e) => {
            e.preventDefault();

            const data = {
                'Email': this.state.email,
                'Password': this.state.password
            }

            axios({
                url: "http://127.0.0.1:8000/api/login",
                method: "POST",
                data: data,
                headers: {
                    'Access-Control-Allow-Origin' : '*',
                    'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
                    'Content-Type' : 'application/json'
                },
            }).then((res) => {
                console.log(res.data)
                localStorage.setItem('userName', res.data.User.Name);
                localStorage.setItem('userID', res.data.User.User_ID);
                window.location.href = '/dashboard';
            }).catch((err) => {
                if(err.response?.data)
                    console.log("Error", err.response.data);
                else
                    console.log("Error", err);
            })
        }

        return (
            <div class="login-wrapper">
                <div id="bg"></div>
                <form class="form">
                    <div class="form-field username">
                        <input
                            type="text"
                            placeholder="Username"
                            value={this.state.email}
                            onChange={setEmail}
                            required
                        />
                    </div>
                    <div class="form-field password">
                        <input
                            type="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={setPassword}
                            required
                        />
                    </div>
                    <p>
                        Don't have an account?{" "}
                        <Link to="/register">Register</Link> now.
                    </p>
                    <div class="form-field">
                        <button class="btn" type="submit" onClick={loginUser}>
                            Log in
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

export default Login;
