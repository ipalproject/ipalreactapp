import React from "react";
import { Link } from "react-router-dom";

class Dashboard extends React.Component {
    state = {
        loading: true,
        data: [],
    };

    componentDidMount() {
        console.log("User => ", localStorage.getItem("userName"));
        if (!("userName" in localStorage))
          window.location.href = "/login";
        this.stopLoading();
    }

    stopLoading() {
        this.setState({ loading: false });
    }

    logOut() {
        localStorage.removeItem("userID");
        localStorage.removeItem("userName");
    }

    render() {
        if (!this.state.loading)
            return (
                <div>
                    <div className="sidebar">
                        <div className="logo-details">
                            <i className="bx bxs-graduation"></i>
                            <span className="logo_name">iPal</span>
                        </div>
                        <ul className="nav-links">
                            <li>
                                <a
                                    href="/#"
                                    tooltip="Dashboard"
                                    className="active"
                                >
                                    <i className="bx bx-grid-alt"></i>
                                    <span className="links_name">
                                        Dashboard
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-book"></i>
                                    <span className="links_name">
                                        Resources
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-abacus"></i>
                                    <Link to="/quiz" className="links_name">
                                        Take a Test
                                    </Link>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-pie-chart-alt-2"></i>
                                    <Link to="/analytics" className="links_name">
                                        Analytics
                                    </Link>
                                </a>
                            </li>
                            <li>
                                <a href="/#">
                                    <i className="bx bx-cog"></i>
                                    <Link to="/improvement" className="links_name">
                                        Improvement Test
                                    </Link>
                                </a>
                            </li>
                            <li className="log_out">
                                <a href="/#">
                                    <i className="bx bx-log-out"></i>
                                    <span className="links_name" onClick={this.logOut}>Log out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <section className="home-section">
                        <nav>
                            <div className="sidebar-button">
                                <span className="dashboard">Dashboard</span>
                            </div>
                            <div className="search-box">
                                <input type="text" placeholder="Search..." />
                                <i className="bx bx-search"></i>
                            </div>
                            <div className="profile-details">
                                <span className="admin_name">{localStorage.getItem("userName")}</span>
                            </div>
                        </nav>
                        <div className="home-content">
                            <div className="overview-boxes">
                                <div className="box">
                                    <div className="right-side">
                                        <div className="box-topic">
                                            Personal %
                                        </div>
                                        <div className="number">74%</div>
                                        <div className="indicator">
                                            <i className="bx bx-up-arrow-alt"></i>
                                            <span className="text">
                                                Up from yesterday
                                            </span>
                                        </div>
                                    </div>
                                    <i className="bx bx-smile cart"></i>
                                </div>
                                <div className="box">
                                    <div className="right-side">
                                        <div className="box-topic">
                                            Average %
                                        </div>
                                        <div className="number">38%</div>
                                        <div className="indicator">
                                            <i className="bx bx-up-arrow-alt"></i>
                                            <span className="text">
                                                Up from yesterday
                                            </span>
                                        </div>
                                    </div>
                                    <i className="bx bx-group cart two"></i>
                                </div>
                                <div className="box">
                                    <div className="right-side">
                                        <div className="box-topic">
                                            Tests Taken
                                        </div>
                                        <div className="number">23</div>
                                        <div className="indicator">
                                            <i className="bx bx-up-arrow-alt"></i>
                                            <span className="text">
                                                Up from yesterday
                                            </span>
                                        </div>
                                    </div>
                                    <i className="bx bx-abacus cart three"></i>
                                </div>
                                <div className="box">
                                    <div className="right-side">
                                        <div className="box-topic">
                                            Today's Hours
                                        </div>
                                        <div className="number">3 hrs</div>
                                        <div className="indicator">
                                            <i className="bx bx-up-arrow-alt"></i>
                                            <span className="text">
                                                Up from yesterday
                                            </span>
                                        </div>
                                    </div>
                                    <i className="bx bx-time cart four"></i>
                                </div>
                            </div>

                            <div className="sales-boxes">
                                <div className="recent-sales box">
                                    <div className="title">Report Card :</div>
                                    <div className="sales-details">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>Subject</th>
                                                    <th>Your Current Level</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Database</td>
                                                    <td>70</td>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <td>AI</td>
                                                    <td>73</td>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <td>Data Structures</td>
                                                    <td>47</td>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <td>Software Engineering</td>
                                                    <td>92</td>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <td>Front End</td>
                                                    <td>74</td>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <td>Operating System</td>
                                                    <td>81</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            );
        else return <div class="loading_screen">
            <div class="loading_image"></div>
        </div>;
    }
}

export default Dashboard;
